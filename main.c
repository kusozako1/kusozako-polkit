// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include <gtk/gtk.h>
#include "application.h"

int
main(int    argc,
     char **argv)
{
    DeltaApplication *application;
    int               exit_status;

    application = delta_application_new ();
    exit_status = g_application_run (G_APPLICATION (application), argc, argv);

    return exit_status;
}
