// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "session-handler.h"

struct _DeltaSessionHandler {
    GObject             parent;
    PolkitAgentSession *session;
};

G_DEFINE_TYPE (
    DeltaSessionHandler,    // new type name in CamelCase
    delta_session_handler,  // new type name in snake_case
    G_TYPE_OBJECT           // parent GType
    );

static void
completed_cb (PolkitAgentSession  *session,
              gboolean             gained_authorization,
              DeltaSessionHandler *self)
{
    if (gained_authorization){
        g_signal_emit_by_name (self, "authorized", NULL);
    } else {
        g_object_unref (G_OBJECT (self->session));
        g_signal_emit_by_name (self, "authorization-failed", NULL);
    }
}

static void
request_cb (PolkitAgentSession *session,
            gchar              *request,
            gboolean            echo_on,
            gpointer            user_data)
{
    // g_print ("requested :: %s\n", request);
}

static void
show_error_cb (PolkitAgentSession *session,
               gchar              *text,
               gpointer            user_data)
{
    g_print ("show-error");
}

static void
show_info_cb (PolkitAgentSession *session,
              gchar              *text,
              gpointer            user_data)
{
    g_print ("show-request");
}

void
delta_session_handler_response (DeltaSessionHandler  *self,
                                const gchar          *password)
{
    polkit_agent_session_response (POLKIT_AGENT_SESSION (self->session),
        password);
}

static void
respawn_session (DeltaSessionHandler  *self,
                 PolkitIdentity       *identity,
                 const gchar          *cookie)
{
    self->session = polkit_agent_session_new (identity, cookie);
    g_signal_connect (self->session, "completed", G_CALLBACK (completed_cb),
        self);
    g_signal_connect (self->session, "request", G_CALLBACK (request_cb),
        NULL);
    g_signal_connect (self->session, "show-error", G_CALLBACK (show_error_cb),
        NULL);
    g_signal_connect (self->session, "show-info", G_CALLBACK (show_info_cb),
        NULL);
    polkit_agent_session_initiate (self->session);
}

static void
delta_session_handler_class_init (DeltaSessionHandlerClass *class)
{
    g_signal_new (
        "authorized",
        G_TYPE_FROM_CLASS (class),
        G_SIGNAL_RUN_LAST,
        0,                          // class_offset
        NULL,                       // accumulator
        NULL,                       // accumulator data
        NULL,                       // c_marshaller
        G_TYPE_NONE,                // return type, returns nothing.
        0                           // number of params
        );
    g_signal_new (
        "authorization-failed",
        G_TYPE_FROM_CLASS (class),
        G_SIGNAL_RUN_LAST,
        0,                          // class_offset
        NULL,                       // accumulator
        NULL,                       // accumulator data
        NULL,                       // c_marshaller
        G_TYPE_NONE,                // return type, returns nothing.
        0                           // number of params
        );
}

static void
delta_session_handler_init (DeltaSessionHandler  *self)
{
    // pass
}

void
delta_session_handler_cancel_session (DeltaSessionHandler  *self)
{
    if (!self->session)
        return;
    polkit_agent_session_cancel (self->session);
    self->session = NULL;
}

void
delta_session_handler_respawn (DeltaSessionHandler  *self,
                               PolkitIdentity       *identity,
                               const gchar          *cookie)
{
    respawn_session (self, identity, cookie);
}

DeltaSessionHandler *
delta_session_handler_new (void)
{
    return g_object_new (DELTA_TYPE_SESSION_HANDLER, NULL);
}
