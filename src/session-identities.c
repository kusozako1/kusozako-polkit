// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "session-identities.h"

struct _DeltaSessionIdentities {
    GObject          parent;
    GListStore      *list_store;
    PolkitIdentity  *current_identity;
};

G_DEFINE_TYPE (
    DeltaSessionIdentities,     // new type name in CamelCase
    delta_session_identities,   // new type name in snake_case
    G_TYPE_OBJECT               // parent GType
    );

static void
delta_session_identities_class_init (DeltaSessionIdentitiesClass  *class)
{
    // pass
}

static void
delta_session_identities_init (DeltaSessionIdentities  *self)
{
    self->list_store = g_list_store_new (POLKIT_TYPE_IDENTITY);
}

PolkitIdentity *
delta_session_identity_get_current (DeltaSessionIdentities  *self)
{
    return self->current_identity;
}

DeltaSessionIdentities *
delta_session_identities_new (GList  *identities)
{
    DeltaSessionIdentities  *self;

    self = g_object_new (DELTA_TYPE_SESSION_IDENTITIES, NULL);
    for (guint i = 0; i < g_list_length (identities); i++) {
        PolkitIdentity *identity = g_list_nth_data (identities, i);
        if (self->current_identity == NULL) {
            self->current_identity = identity;
        }
        g_list_store_append (self->list_store, identity);
    }

    return self;
}
