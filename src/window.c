// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include <gsound.h>
#include "window.h"

struct _DeltaWindow
{
    GtkWindow        parent;
    GtkWidget       *icon_image;
    GtkWidget       *message_label;
    GtkWidget       *password_entry;
    GtkWidget       *cancel_button;
    GtkWidget       *authenticate_button;
    GSoundContext   *sound_context;
};

G_DEFINE_TYPE(
    DeltaWindow,
    delta_window,
    GTK_TYPE_WINDOW
    );

static void
try_activate_cb(GtkWidget    *source_widget,
                DeltaWindow  *self)
{
    guint16       length;
    const gchar  *password;

    length = gtk_entry_get_text_length (GTK_ENTRY (self->password_entry));
    if (length == 0)
        return;
    password = gtk_entry_get_text (GTK_ENTRY (self->password_entry));
    g_signal_emit_by_name (self, "password-received", password);
}

static void
cancel_cb(GtkWidget    *cancel_button,
          DeltaWindow  *self)
{
    gtk_widget_hide (GTK_WIDGET (self));
}

static void
password_changed_cb (GtkEntry     *entry,
                     DeltaWindow  *self)
{
    guint16  length;

    length = gtk_entry_get_text_length (GTK_ENTRY (self->password_entry));
    gtk_widget_set_sensitive (GTK_WIDGET (self->authenticate_button),
        (length > 0));
}

static void
hide_cb (GtkWidget    *source_widget,
         DeltaWindow  *self)
{
    gtk_entry_set_text (GTK_ENTRY (self->password_entry), "");
    gtk_widget_grab_focus (GTK_WIDGET (self->password_entry));
    gsound_context_play_simple (self->sound_context, NULL, NULL,
        GSOUND_ATTR_EVENT_ID, "window-attention",
        NULL);
    g_signal_emit_by_name (self, "abort-authentication", NULL);
}

static gboolean
delete_event_cb (GtkWidget    *source_widget,
                 GdkEvent     *event,
                 DeltaWindow  *self)
{
    gsound_context_play_simple (self->sound_context, NULL, NULL,
        GSOUND_ATTR_EVENT_ID, "dialog-warning",
        NULL);

    return gtk_widget_is_visible (GTK_WIDGET (source_widget));
}

static gboolean
event_cb (GtkWidget    *event_source,
          GdkEvent     *event,
          DeltaWindow  *self)
{
    if (event->type != GDK_KEY_PRESS)
        return FALSE;
    if (event->key.keyval != 65307)     // 65307 is escape-key
        return FALSE;
    gtk_widget_hide (GTK_WIDGET (self));

    return FALSE;
}

static gboolean
timeout_cb (gpointer  user_data)
{
    gtk_widget_hide (GTK_WIDGET (user_data));

    return G_SOURCE_REMOVE;
}

static void
delta_window_class_init (DeltaWindowClass *class)
{
    gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
        "/com/gitlab/kusozako1/Polkit/window.ui");
    gtk_widget_class_bind_template_child_internal (GTK_WIDGET_CLASS (class),
        DeltaWindow, icon_image);
    gtk_widget_class_bind_template_child_internal (GTK_WIDGET_CLASS (class),
        DeltaWindow, message_label);
    gtk_widget_class_bind_template_child_internal (GTK_WIDGET_CLASS (class),
        DeltaWindow, password_entry);
    gtk_widget_class_bind_template_child_internal (GTK_WIDGET_CLASS (class),
        DeltaWindow, cancel_button);
    gtk_widget_class_bind_template_child_internal(GTK_WIDGET_CLASS (class),
        DeltaWindow, authenticate_button);
    g_signal_new (
        "password-received",
        G_TYPE_FROM_CLASS (class),
        G_SIGNAL_RUN_LAST,
        0,                          // class_offset
        NULL,                       // accumulator
        NULL,                       // accumulator data
        NULL,                       // c_marshaller
        G_TYPE_NONE,                // return type, returns nothing.
        1,                          // number of params
        G_TYPE_STRING               // means gchar
        );
    g_signal_new (
        "abort-authentication",
        G_TYPE_FROM_CLASS (class),
        G_SIGNAL_RUN_LAST,
        0,                          // class_offset
        NULL,                       // accumulator
        NULL,                       // accumulator data
        NULL,                       // c_marshaller
        G_TYPE_NONE,                // return type, returns nothing.
        0                           // number of params
        );

}

static void
delta_window_init (DeltaWindow *self)
{
    gtk_widget_init_template (GTK_WIDGET (self));
    gtk_window_set_keep_above (GTK_WINDOW (self), TRUE);
    g_signal_connect (GTK_WIDGET (self), "delete-event",
        G_CALLBACK (delete_event_cb), self);
    g_signal_connect (GTK_WIDGET (self), "hide",
        G_CALLBACK (hide_cb), self);
    g_signal_connect (GTK_WIDGET (self), "event",
        G_CALLBACK (event_cb), self);
    g_signal_connect (self->password_entry, "activate",
        G_CALLBACK (try_activate_cb), self);
    g_signal_connect (self->cancel_button, "clicked",
        G_CALLBACK (cancel_cb), self);
    g_signal_connect (self->authenticate_button, "clicked",
        G_CALLBACK (try_activate_cb), self);
    g_signal_connect (self->password_entry, "changed",
        G_CALLBACK (password_changed_cb), self);
    self->sound_context = gsound_context_new (NULL, NULL);
}

void
delta_window_set_message (DeltaWindow  *self,
                          const gchar  *message)
{
    gtk_label_set_text (GTK_LABEL (self->message_label), message);
    gtk_editable_delete_text (GTK_EDITABLE (self->password_entry), 0, -1);
    gtk_widget_show_all (GTK_WIDGET (self));
    gsound_context_play_simple (self->sound_context, NULL, NULL,
        GSOUND_ATTR_EVENT_ID, "window-question",
        NULL);
    g_timeout_add_seconds (25, timeout_cb, self);
}

void
delta_window_set_failed (DeltaWindow  *self)
{
    gtk_label_set_text (GTK_LABEL (self->message_label),
        "Wrong Password. Try Again.");
    gtk_entry_set_text (GTK_ENTRY (self->password_entry), "");
    gsound_context_play_simple (self->sound_context, NULL, NULL,
        GSOUND_ATTR_EVENT_ID, "service-logout",
        NULL);
}

DeltaWindow *
delta_window_new (void)
{
    return g_object_new (DELTA_TYPE_WINDOW, NULL);
}
