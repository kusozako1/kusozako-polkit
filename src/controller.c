// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "controller.h"
#include "session-data.h"
#include "listener.h"
#include "window.h"

struct _DeltaController {
    GObject              parent;
    DeltaListener       *listener;
    DeltaWindow         *window;
    DeltaSessionData    *session_data;
};

G_DEFINE_TYPE (
    DeltaController,    // new type name in CamelCase
    delta_controller,   // new type name in snake_case
    G_TYPE_OBJECT       // parent GType
    );

static void
authentication_finished_cb (DeltaSessionData  *source_object,
                            DeltaController   *self)
{
    gtk_widget_hide (GTK_WIDGET (self->window));
    g_object_unref (G_OBJECT (self->session_data));
}

static void
wrong_password_cb (DeltaSessionData  *source_object,
                   DeltaController   *self)
{
    delta_window_set_failed (self->window);
}

static void
authentication_initiated_cb (DeltaListener     *listener,
                             DeltaSessionData  *session_data,
                             DeltaController   *self)
{
    const gchar  *message;

    message = delta_session_data_get_message (
        DELTA_SESSION_DATA (session_data));
    delta_window_set_message (DELTA_WINDOW (self->window), message);
    self->session_data = session_data;
    delta_session_data_generate_session (DELTA_SESSION_DATA (session_data), 0);
    g_signal_connect (self->session_data, "authentication-finished",
        G_CALLBACK (authentication_finished_cb), self);
    g_signal_connect (self->session_data, "wrong-password",
        G_CALLBACK (wrong_password_cb), self);
}

static void
password_received_cb (DeltaWindow      *window,
                      const gchar      *password,
                      DeltaController  *self)
{
    delta_session_data_response (self->session_data, password);
}

static void
abort_authentication_cb (DeltaWindow      *window,
                         DeltaController  *self)
{
    delta_session_data_cancel_session (self->session_data);
}

static void
delta_controller_class_init (DeltaControllerClass  *class)
{
    // pass
}

static void
delta_controller_init (DeltaController  *self)
{
    self->window = delta_window_new ();
    g_signal_connect (self->window, "password-received",
        G_CALLBACK (password_received_cb), self);
    g_signal_connect (self->window, "abort-authentication",
        G_CALLBACK (abort_authentication_cb), self);
    self->listener = delta_listener_new ();
    g_signal_connect (self->listener, "authentication-initiated",
        G_CALLBACK (authentication_initiated_cb), self);
}

DeltaController *
delta_controller_new (void)
{
    return g_object_new (DELTA_TYPE_CONTROLLER, NULL);
}
