// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "listener.h"
#include "session-data.h"

struct _DeltaListener {
    PolkitAgentListener   parent;
};

G_DEFINE_TYPE(
    DeltaListener,
    delta_listener,
    POLKIT_AGENT_TYPE_LISTENER
    );

static void
initiate_authentication (PolkitAgentListener  *listener,
                         const gchar          *action_id,       // unused
                         const gchar          *message,
                         const gchar          *icon_name,       // unused
                         PolkitDetails        *details,         // unused
                         const gchar          *cookie,
                         GList                *identities,
                         GCancellable         *cancellable,
                         GAsyncReadyCallback   callback,
                         gpointer              user_data)
{
    DeltaSessionData    *session_data;

    session_data = delta_session_data_new (listener, message, cookie,
        identities, cancellable, callback, user_data);
    g_signal_emit_by_name (DELTA_LISTENER (listener),
        "authentication-initiated", session_data);
}

static gboolean
initiate_authentication_finish (PolkitAgentListener  *listener,
                                GAsyncResult         *task,
                                GError              **error)
{
    return !g_task_propagate_boolean (G_TASK (task), error);
}

static void
finalize (GObject *object)
{
    g_return_if_fail (object != NULL);
    g_return_if_fail (DELTA_IS_LISTENER (object));
    G_OBJECT_CLASS (delta_listener_parent_class)->finalize (object);
}

static void
delta_listener_class_init (DeltaListenerClass  *class)
{
    G_OBJECT_CLASS (class)->finalize = finalize;
    POLKIT_AGENT_LISTENER_CLASS (class)->initiate_authentication
        = initiate_authentication;
    POLKIT_AGENT_LISTENER_CLASS (class)->initiate_authentication_finish
        = initiate_authentication_finish;
    g_signal_new (
        "authentication-initiated",
        G_TYPE_FROM_CLASS (class),
        G_SIGNAL_RUN_LAST,
        0,                          // class_offset
        NULL,                       // accumulator
        NULL,                       // accumulator data
        NULL,                       // c_marshaller
        G_TYPE_NONE,                // return type, returns nothing.
        1,                          // number of params
        G_TYPE_OBJECT               // see session-data.c
        );

}

static void
delta_listener_init (DeltaListener  *self)
{
    // pass
}

DeltaListener *
delta_listener_new (void)
{
    DeltaListener  *self;
    PolkitSubject  *subject;
    GError         *error = NULL;

    self = g_object_new (DELTA_TYPE_LISTENER, NULL);
    subject = polkit_unix_session_new_for_process_sync (getpid(), NULL, NULL);
    polkit_agent_listener_register (POLKIT_AGENT_LISTENER (self),
        POLKIT_AGENT_REGISTER_FLAGS_NONE, subject, NULL, NULL, &error);

    return self;
}
