// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include <gtk/gtk.h>
#include "application.h"
#include "controller.h"

struct _DeltaApplication
{
    GtkApplication parent;
};

G_DEFINE_TYPE(
    DeltaApplication,       // new type name in CamelCase
    delta_application,      // new type name in snake_case
    GTK_TYPE_APPLICATION    // parent GType
    );


static void
activate (GApplication *application)
{
    delta_controller_new ();
    g_application_hold (G_APPLICATION (application));
}

static void
delta_application_class_init (DeltaApplicationClass *class)
{
    G_APPLICATION_CLASS (class)->activate = activate;
}

static void
delta_application_init (DeltaApplication *self)
{
	// pass
}

DeltaApplication *
delta_application_new (void)
{
    return g_object_new (DELTA_TYPE_APPLICATION,
        // null terminated key-value pairs for propeties.
        "application-id", "com.gitlab.kusozako1.Polkit",
        "flags",          G_APPLICATION_FLAGS_NONE,
        NULL);
}
