// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "session-data.h"
#include "session-handler.h"
#include "session-identities.h"
#include "listener.h"

struct _DeltaSessionData{
    GObject                  parent;
    const gchar             *message;
    const gchar             *cookie;
    DeltaSessionIdentities  *identities;
    DeltaSessionHandler     *session_handler;
    GTask                   *task;
};

G_DEFINE_TYPE(
    DeltaSessionData,   // new type name in CamelCase
    delta_session_data, // new type name in snake_case
    G_TYPE_OBJECT       // parent GType
    );

static void
authorized_cb (DeltaSessionHandler  *source_object,
               DeltaSessionData     *self)
{
    if (g_task_get_completed (self->task))
        return;
    g_task_return_pointer (G_TASK (self->task), NULL, NULL);
    g_signal_emit_by_name (self, "authentication-finished", NULL);
}

static void
authorization_failed_cb (DeltaSessionHandler  *source_object,
                         DeltaSessionData     *self)
{
    delta_session_handler_respawn (self->session_handler,
        delta_session_identity_get_current (self->identities),
        self->cookie);
    g_signal_emit_by_name (self, "wrong-password", NULL);
}

static void
delta_session_data_class_init (DeltaSessionDataClass  *class)
{
    g_signal_new (
        "authentication-finished",
        G_TYPE_FROM_CLASS (class),
        G_SIGNAL_RUN_LAST,
        0,                          // class_offset
        NULL,                       // accumulator
        NULL,                       // accumulator data
        NULL,                       // c_marshaller
        G_TYPE_NONE,                // return type, returns nothing.
        0                           // number of params
        );
    g_signal_new (
        "wrong-password",
        G_TYPE_FROM_CLASS (class),
        G_SIGNAL_RUN_LAST,
        0,                          // class_offset
        NULL,                       // accumulator
        NULL,                       // accumulator data
        NULL,                       // c_marshaller
        G_TYPE_NONE,                // return type, returns nothing.
        0                           // number of params
        );
}

static void
delta_session_data_init (DeltaSessionData  *self)
{
    self->session_handler = delta_session_handler_new ();
    g_signal_connect(self->session_handler, "authorized",
        G_CALLBACK (authorized_cb), self);
    g_signal_connect(self->session_handler, "authorization-failed",
        G_CALLBACK (authorization_failed_cb), self);
}

void
delta_session_data_cancel_session (DeltaSessionData  *self)
{
    if (g_task_get_completed (self->task))
        return;
    delta_session_handler_cancel_session (self->session_handler);
    g_task_return_pointer (G_TASK (self->task), NULL, NULL);
}

void
delta_session_data_response (DeltaSessionData *self,
                             const gchar       *password)
{
    delta_session_handler_response (self->session_handler, password);
}


const gchar *
delta_session_data_get_message (DeltaSessionData *self)
{
    return self->message;
}

void
delta_session_data_generate_session (DeltaSessionData  *self,
                                     int                nth)
{
    delta_session_handler_respawn (
        DELTA_SESSION_HANDLER (self->session_handler),
        delta_session_identity_get_current (self->identities),
        self->cookie);
}

DeltaSessionData *
delta_session_data_new (PolkitAgentListener  *listener,
                        const gchar          *message,
                        const gchar          *cookie,
                        GList                *identities,
                        GCancellable         *cancellable,
                        GAsyncReadyCallback   callback,
                        gpointer              user_data)
{
    DeltaSessionData  *self;

    self = g_object_new (DELTA_TYPE_SESSION_DATA, NULL);
    self->message = message;
    self->cookie = cookie;
    self->identities = delta_session_identities_new (identities);
    self->task = g_task_new (listener, cancellable, callback, user_data);

    return self;
}
