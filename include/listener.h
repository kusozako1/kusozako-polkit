// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once
#include "polkit-agent.h"
// I don't know why, G_DECLARE_FINAL_TYPE doen't work.
// that causes error: ‘glib_autoptr_clear_PolkitAgentListener’ undeclared
#define DELTA_TYPE_LISTENER (delta_listener_get_type())
#define DELTA_LISTENER(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),              \
    DELTA_TYPE_LISTENER, DeltaListener))
#define DELTA_LISTENER_CLASS(class_) (G_TYPE_CHECK_CLASS_CAST((class_),     \
    DELTA_TYPE_LISTENER, DeltaListenerClass))
#define DELTA_IS_LISTENER(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),           \
    DELTA_TYPE_LISTENER))
#define DELTA_IS_LISTENER_CLASS(class_) (G_TYPE_CHECK_CLASS_TYPE((class_),  \
    DELTA_TYPE_LISTENER))
#define DELTA_LISTENER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj),     \
    DELTA_TYPE_LISTENER, DeltaListenerClass))


typedef struct _DeltaListener DeltaListener;
typedef struct _DeltaListenerClass DeltaListenerClass;

struct _DeltaListenerClass {
    PolkitAgentListenerClass parent_class;
};


GType
delta_listener_get_type (void);

DeltaListener *
delta_listener_new (void);
