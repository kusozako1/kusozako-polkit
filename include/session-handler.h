// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once
#include "listener.h"
#define DELTA_TYPE_SESSION_HANDLER (delta_session_handler_get_type ())

G_DECLARE_FINAL_TYPE (
    DeltaSessionHandler,    // module_object name in CamelCase
    delta_session_handler,  // module_object name in snake_case
    DELTA,                  // name of module in ALL_CAPS
    SESSION_HANDLER,        // bare name of TYPE
    GObject                 // parent type
    )

void
delta_session_handler_cancel_session (DeltaSessionHandler  *self);

void
delta_session_handler_response (DeltaSessionHandler  *self,
                                const gchar          *password);

void
delta_session_handler_respawn (DeltaSessionHandler  *self,
                               PolkitIdentity       *identity,
                               const gchar          *cookie);

DeltaSessionHandler *
delta_session_handler_new (void);
