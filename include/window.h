// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once
#include <gtk/gtk.h>
#define DELTA_TYPE_WINDOW (delta_window_get_type ())

G_DECLARE_FINAL_TYPE (
    DeltaWindow,    // module_object name in CamelCase
    delta_window,   // module_object name in snake_case
    DELTA,          // name of module in ALL_CAPS
    WINDOW,         // bare name of TYPE
    GtkWindow       // parent type
    )

void
delta_window_set_message (DeltaWindow  *self,
                          const gchar  *message);

void
delta_window_set_failed (DeltaWindow  *self);

DeltaWindow *
delta_window_new (void);
