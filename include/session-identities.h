// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once
#include "listener.h"
#define DELTA_TYPE_SESSION_IDENTITIES (delta_session_identities_get_type ())

G_DECLARE_FINAL_TYPE (
    DeltaSessionIdentities,     // module_object name in CamelCase
    delta_session_identities,   // module_object name in snake_case
    DELTA,                      // name of module in ALL_CAPS
    SESSION_IDENTITIES,         // bare name of TYPE
    GObject                     // parent type
    )


PolkitIdentity *
delta_session_identity_get_current (DeltaSessionIdentities  *self);

DeltaSessionIdentities *
delta_session_identities_new (GList  *identities);
