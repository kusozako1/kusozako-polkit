// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once
#define POLKIT_AGENT_I_KNOW_API_IS_SUBJECT_TO_CHANGE
#include <polkitagent/polkitagent.h>
