// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once
#include <gtk/gtk.h>
#include "listener.h"
#define DELTA_TYPE_SESSION_DATA (delta_session_data_get_type ())

G_DECLARE_FINAL_TYPE (
    DeltaSessionData,   // module_object name in CamelCase
    delta_session_data, // module_object name in snake_case
    DELTA,              // name of module in ALL_CAPS
    SESSION_DATA,       // bare name of TYPE
    GObject             // parent type
    )


void
delta_session_data_cancel_session (DeltaSessionData  *self);

void
delta_session_data_response (DeltaSessionData  *self,
                             const gchar       *password);

const gchar *
delta_session_data_get_message (DeltaSessionData  *self);

void
delta_session_data_generate_session (DeltaSessionData  *self,
                                     int                nth);

DeltaSessionData *
delta_session_data_new (PolkitAgentListener  *listener,
                        const gchar          *message,
                        const gchar          *cookie,
                        GList                *identities,
                        GCancellable         *cancellable,
                        GAsyncReadyCallback   callback,
                        gpointer              user_data);
