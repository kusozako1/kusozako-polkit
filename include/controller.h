// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once
#include <gtk/gtk.h>
#define DELTA_TYPE_CONTROLLER (delta_controller_get_type ())

G_DECLARE_FINAL_TYPE (
    DeltaController,    // module_object name in CamelCase
    delta_controller,   // module_object name in snake_case
    DELTA,              // name of module in ALL_CAPS
    CONTROLLER,         // bare name of TYPE
    GObject             // parent type
    )


DeltaController *
delta_controller_new (void);
