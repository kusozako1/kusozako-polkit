// (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once
#include <gtk/gtk.h>
#define DELTA_TYPE_APPLICATION (delta_application_get_type ())

G_DECLARE_FINAL_TYPE (
    DeltaApplication,   // module_object name in CamelCase
    delta_application,  // module_object name in snake_case
    DELTA,              // name of module in ALL_CAPS
    APPLICATION,        // bare name of TYPE
    GtkApplication      // parent type
    )


DeltaApplication *
delta_application_new (void);
